#!/bin/bash

DEPENDENCIES="git ansible python3-apt"
REPO="https://gricad-gitlab.univ-grenoble-alpes.fr/phitem/environnement-bureau-gnome.git"

install_dependencies() {
	for pkg in $DEPENDENCIES; do
		echo -n "Checking dependency $pkg : "
		if ! dpkg -l $pkg  &>/dev/null; then
			echo "installing $pkg..."
			sudo apt-get install -y $pkg
		else
			echo "Ok"
		fi
	done
}

main() {
	install_dependencies
	git clone $REPO
	cd environnement-bureau-gnome
	ANSIBLE_LOCALHOST_WARNING=false ansible-playbook gnome.yml
	cd -
	rm -rf environnement-bureau-gnome
}

main $@
