# Installation environnement bureau Gnome avec Ansible

Ce rôle Ansible installe et configure un environnement bureau GNOME de base FR :
  * installe uniquement _gnome-session_ (version minimale de gnome-desktop) et quelques outils de base gnome.  
  * afin de pouvoir configurer facilement un environnement bureau léger (à la différence, par exemple, d'un _ubuntu-desktop_) sur un _Ubuntu server_ ou un Ubuntu minimal (sans ou avec un autre environnement graphique).

## Rapporter un bug ou poser une question

Pour toute question, merci d'utiliser l'[issue tracker](https://gricad-gitlab.univ-grenoble-alpes.fr/phitem/installation-environnement-bureau-gnome-avec-ansible/-/issues).

## Prérequis

* Une installation de _Ubuntu Server_ ou d'un Ubuntu minimal (sans ou avec un environnement graphique non-gnome).
* Une installation d'Ansible.

## Utilisation

### Setup

Installer Ansible et python :

```bash
sudo apt install ansible python3-apt
```

Cloner le dépôt du gricad-gitlab :

```bash
git clone "https://gricad-gitlab.univ-grenoble-alpes.fr/phitem/environnement-bureau-gnome.git"
```

```bash
cd environnement-bureau-gnome
```

Modifier les fichiers de configuration `vars/main.yml` et `gnome.yml` pour les adapter à votre contexte.

Si vous voulez installer d'autres logiciels (Firefox, VLC, libreoffice, etc.) sur Gnome, il suffit de modifier la variable `extra_apps` dans `vars/main.yml`.

### Appliquer la configuration

1. Pour vérifier le code et voir quelles modifications seront apportées :

```bash
ansible-playbook gnome.yml --check
```

2. Appliquer la configuration :

```bash
ansible-playbook gnome.yml

```

## Installation rapide

Télécharger et lancer [install.sh](install.sh)

Exemple :

```bash
curl "https://gricad-gitlab.univ-grenoble-alpes.fr/phitem/environnement-bureau-gnome/-/raw/master/install.sh" | bash
```
